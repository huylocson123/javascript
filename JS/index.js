/* 
input
    - số ngày 
    - tiền lương 1 ngày
process
    - tao bien day
    - tao bien luong
    - tao bien tongluong
    - tong luong = day * luong
out put
    - tongluong
*/

var day = 8,
    luong = 100000,
    tongLuong = day * luong;
console.log("Tổng lương :", tongLuong);


/* 
input
    -  5 số thực

process 
    - tạo 5 biến num1 num2 num3 num4 num5
    - tạo biến kq
    - tổng 5 số thực / 5
out put
    -giá trị trung bình
*/

var num1 = 1,
    num2 = 2,
    num3 = 3,
    num4 = 4,
    num5 = 5;
var kq = (num1 + num2 + num3 + num4 + num5) / 5;
console.log("kết quả trung bình:", kq);

/* 
input
    - số tiền
    - vnd = 245000

process
    - tạo biến USD
    - tạo biến VND = 245000
    -tao biến giá tri = sotien USD * 245000
out put
    - giá trị quy đổi
*/

var USD = 5,
    VND = 245000,
    quyDoi = USD * VND;
console.log("Giá trị quy đổi:", quyDoi)

/* 
input
    - chiều dài
    - chiều rộng 
    - diện tích 
    -  chu vi
process
    -tạo biến dai
    -tạo biến rong
    -tạo biến S
    -tạo biến V
    S = dai * rong 
    V = (dai + rong )* 2
out put
    S, V
*/
var dai = 4,
    rong = 5,
    S = dai * rong,
    V = (dai + rong) * 2;
console.log("Diện tích:", S);
console.log("Chu vi:", V);



/* 
input
    -1 số có 2 chữ số
process
    - tạo biến so
    - tao bien so nguyen hangDv
    - tao bien so nguyen hangC
    - handDv = so % 10
    - handC = so / 10
    - tao bien kq =hangDv + hangC
out put
    - in ra màn hình tổng 2 kí số 
*/
var so = 25,
    hangDv = parseInt(so % 10),
    hangC = parseInt(so / 10),
    kq = hangDv + hangC;
console.log("Tổng 2 kí số :", kq)